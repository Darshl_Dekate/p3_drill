
const assignNew = require('./index')
// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
const output3 = assignNew.map(object => {

    return { ...object, cvv: Math.floor((Math.random() * 1000) + 1) };
});
console.log(output3)